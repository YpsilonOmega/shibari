# Curves ahead TK

## Description

This TK variant features a numbers of decoratively woven crossing knots. The back is stabilized with a series of [X frictions](../../knot/frix/).

## Instructions

#. Bring the forearms horizontally behind the back and tie them with a single column tie.
#. Continue over the left arm, closely under the shoulder, towards the front at A.
#. Wrap around the upper part of the chest from A to B.
#. Cross over the right arm to the back at B.
#. On the back, cross over the strand coming from the single column tie, and continue to the front over the left arm closely under A at C.
#. Wrap around the upper part of the chest from C to D, closely under the previous wrap.
#. Cross over the right arm to the back at D.
#. On the back, cross over the strand coming from the single column tie, and continue to the front over the left arm closely under C at E.
#. Wrap around the upper part of the chest from E to F, closely under the previous wrap.
#. Cross over the right arm to the back at F.
#. On the back, cross under the strand coming from the single column tie, and pull it up so that it becomes vertical.
#. Cross diagonally over all ropes from the bottom left to the top right.
#. Cross down behind the strands BDF.
#. Cross diagonally over all ropes from the bottom right to the top left.
#. Cross down behind the strands ACE.
#. Cross to the right over the vertical stem, then back to the left under it.
#. The result should resemble the following X friction pattern:

	![First X friction](back1.png)

#. Continue towards the front over the left arm at G.
#. Wrap around the lower part of the chest from G to H.
#. Cross over the right arm to the back at H.
#. On the back, cross under the vertical stem, and continue to the front over the left arm closely under G at I.
#. Wrap around the lower part of the chest from I to J.
#. Cross over the right arm to the back at J.
#. On the back, cross towards the left under the vertical stem.
#. Cross diagonally over the vertical stem and the lower chest ropes from the bottom left to the top right.
#. Cross down behind the strands HJ.
#. Cross diagonally over the vertical stem and the lower chest ropes from the bottom right to the top left.
#. Cross down behind the strands GI.
#. Cross to the right over the vertical stem, then back to the left under it.
#. The result should resemble the following X friction pattern:

	![Second X friction](back2.png)

#. Pass the ropes to the front under the left arm at K.
#. In the front, cross down over the strands G-H and I-J.
#. Pass back to the back under the left arm at L.
#. On the back, cross to the right under the vertical stem.
#. Pass the ropes to the front under the right arm at M.
#. In the front, cross down over the strands G-H and I-J.
#. Pass back to the back under the left arm at N.
#. On the back, cross to the left under the vertical stem.
#. Cross diagonally over the vertical stem and the last two front ropes from the bottom left to the top right.
#. Cross down behind the strands MN.
#. Cross diagonally over the vertical stem and the last two front ropes from the bottom right to the top left.
#. Cross down behind the strands KL.
#. Cross to the right over the vertical stem, then back to the left under it.
#. The result should resemble the following X friction pattern:

	![Third X friction](back3.png)

#. Pass the ropes to the front under the left arm at O.
#. Cross upward in front of the strands G-H and I-J, on the outer side next to the strand K-L.
#. Continue upwards and cross up over E-F, under C-D and over A-B.
#. Turn inwards and cross down under A-B, over C-D and under E-F.
#. Cross outward over the vertical strand coming from O.
#. Cross up under E-F, over C-D and under A-B towards the left shoulder at P.
#. On the back, cross diagonally down right, then down over B, under D and over F.
#. Turn outwards and then up under F, over D and under B.
#. Cross inward over the strand coming from P.
#. Cross down under B, over D and under F.
#. Cross further down over HJ, then back up behind HJ, vertically inward and down behind HJ again, forming a crossing knot (ABOK #1173).
#. Pass the ropes to the front under the right arm at Q.
#. Cross upward in front of the strands G-H and I-J, on the outer side next to the strand M-N.
#. Continue upwards and cross up over E-F, under C-D and over A-B.
#. Turn inwards and cross down under A-B, over C-D and under E-F.
#. Cross outward over the vertical strand coming from O.
#. Cross up under E-F, over C-D and under A-B towards the right shoulder at R.
#. The front will resemble the following figure:

	![Front view](front.png)

#. On the back, cross over the strand from P, then back under it, over the strand from R and once again under the strand from P, forming a crossing knot (ABOK #1173).
#. Continue diagonally down left, then cross down over A, under C and over E.
#. Turn outwards and then up under E, over C and under A.
#. Cross inward over the strand coming from R.
#. Cross down under A, over C and under E.
#. Cross further down over GI, then back up behind GI, vertically inward and down behind GI again, forming a crossing knot (ABOK #1173).
#. Pass the ropes to the right through the single column tie which surrounds the arms.
#. Cross upward over the strands  HJ.
#. Cross diagonally down left behind the crossing knot on the strands HJ.
#. Cross horizontally outward over the two vertical strands.
#. Cross upward under the strands HJ and further left behind the vertical stem.
#. Cross upward over the strands ACE, then down behind them and up in front again, forming a half hitch (ABOK #48).
#. The final pattern on the back should resemble the following figure:

	![Back view](back4.png)

## References

* <http://www.theduchy.com/curves-ahead-tk/>
