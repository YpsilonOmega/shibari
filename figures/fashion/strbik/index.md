# String bikini

## Description

These are a few examples how to create a woven, triangular bikini top together with attachment strings. The pattern can be woven away from the body either with single or double strands, densely or loosely with a certain amount of transparency, and then attached to the body in different ways, depending on the amount of excess rope.

## Instructions

### Front variants

#### Simple front

#. Take two ropes of equal length, put them next to each other and place them with their common center behind the neck, with a pair of ends running over each shoulder B and C to the front.
#. Cross the ropes in the center of the lower part of the chest, where the connection between the two triangles halves is supposed to be, to measure the necessary length of the rope.
#. To maintain the crossing point, temporarily attach the ropes to each other with a short piece of rope or tape.
#. Remove the rope from the body.
#. On each side, create the triangle shape by weaving following the pattern shown in the picture. To keep the stability of the wraps, it can be helpful to place a chopstick in the center diagonal, where the rope is supposed to come out. The number of weaves can be adjusted to the desired size.
#. When the shape is finished, remove the temporary attachment, place the bight B-C around the neck and run the ends A and D around the lower part of the chest to the back.

	![Simple front style.](simple.png)

#### Square knot front

Instead of simply running the connecting ropes in the center around each other, also a square knot (ABOK #1204) can be used. In this case no temporary attachment is needed. This creates an additional space between the two triangles.

![Square knot front style.](sqknot.png)

#### Double coin knot front

As an alternative to the previous version, a double coin knot (ABOK #1428) can be used to connect the center.

![Double coin knot front style.](dcknot.png)

### Back side attachments

#### Light attachment

If one is short in rope, the ends from A and D can simply be connected with a square knot (ABOK #1204), or with some additional twists, similar to a surgeon's knot (ABOK #1209).

![Light attachment.](backlight.png)

#### Wrapped stem

To use up excess rope, one may create a wrapped stem as follows:

#. Take the ends from A and D on the back and fix them with a square knot (ABOK #1204).
#. Run the rope ends upwards and trough the bight B-C.
#. Wrap the rope ends around the vertical stem.
#. To lock the tie, cross the rope ends down on opposite sides of the original square knot, and fix them with another square knot (ABOK #1204).

	![Wrapped stem style.](backwrap.png)

#### Braided stem

#. Take the ends from A and D on the back and fix them with a square knot (ABOK #1204).
#. Run the rope ends upwards and trough the bight B-C.
#. Wrap the rope ends once around the vertical stem and pull them trough the loops formed around the bight B-C.
#. Reverse direction, passing again in front and behind the vertical stem, and pull through the loops formed in the previous step.
#. Repeat the pattern until all rope is used up.

	![Braided stem style.](backbraid.png)

### Single strand version

Instead of tying everything with two ropes, one may also use a single rope. In this case one either needs a longer rope to achieve the same size of densely woven triangles, or one can weave them more loosely, to achieve a semi-transparent pattern.

## References

* <http://www.theduchy.com/string-bikini/>
