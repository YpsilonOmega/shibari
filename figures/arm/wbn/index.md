# Wrists behind the neck

## Description

This is a quick and simple tie, which can be used as a restraint and tied in different variations.

## Instructions

### Simple version

The common pattern is shown for the most simple version.

#. Start with a double column tie on the wrists.
#. Move the wrists behind the neck.
#. Run the rope ends downward, then make a right angle and lay them once around the lower part of the chest from A to A.
#. Pull the rope ends through the right angled bend, and adjust the rope so that the strand from the wrists to the wrap is vertical and centered.
#. Make another wrap around the chest in the opposite direction, immediately under the first one from B to B.
#. Run the rope ends through the previously created bight A-B.
#. Reverse direction, and lock the tie with a half hitch (ABOK #48) on the other side, so that it will take the pattern shown in the figure.

![Simple version.](simple.png)

### Slipped version

If the tie is meant to be for a short time only and untied soon after, one may also tie a slipped half hitch (ABOK #52) instead of the half hitch (ABOK #48) in the last step.

![Slipped version.](slipped.png)

### Version with additional tightening

Another variant is to continue after tying the simple version, and use any further rope to tighten the wrist tie.

#. Run the rope ends up and behind the double column tie on the wrists.
#. Bring the rope ends to the front between the forearms and run them down again in front of the double column tie.
#. Pull the ropes as tight as desired, creating more tension on the wrists.
#. Cross down over the horizontal lines running towards A and B.
#. Cross up again behind the horizontal lines, on the side where the initial half hitch (ABOK #48) was tied.
#. Lock the tie by making another half hitch around the vertical bundle. This will look as in the figure below.

![Version with additional tightening.](tight.png)

## References

* <http://crash-restraint.com/ties/26>
