# Hip weave

## Description

This hip tie creates a decorative pattern by repeatedly weaving around the hips.

## Instructions

#. Start with the center bight on the back and make a loop around the waist from A to B.
#. Pull through the center bight, thus creating a lark's head (ABOK #5), and reverse direction towards D<sub>1</sub>.
#. On the front, cross diagonally towards the left hip G<sub>1</sub>.
#. Cross behind the left leg and pass to the front between the legs at H<sub>1</sub>.
#. Cross up diagonally in front of the left leg and the strand D<sub>1</sub>-G<sub>1</sub> towards E<sub>1</sub>.
#. Cross behind the back right over the [posterior superior iliac spine](http://en.wikipedia.org/wiki/Posterior_superior_iliac_spine) towards F<sub>1</sub>.
#. Cross diagonally down over the right leg and pass to the back between the legs at I<sub>1</sub>.
#. Cross behind the right leg towards J<sub>1</sub>.
#. Pass to the front again and cross diagonally over the strand F<sub>1</sub>-I<sub>1</sub> and under the strand D<sub>1</sub>-G<sub>1</sub> towards C<sub>1</sub>.
#. On the back, pass through the bight B-D<sub>1</sub> and change direction from C<sub>1</sub> to C<sub>2</sub>.
#. From now on, follow the previous rope in opposite direction and weave: cross over when you went under, cross under when you went over.
#. On the front, go diagonally to J<sub>2</sub>, crossing over the strand D<sub>1</sub>-G<sub>1</sub> and under the strand F<sub>1</sub>-I<sub>1</sub>.
#. Cross behind the right leg towards I<sub>2</sub> and pass to the front between the legs.
#. On the front, cross diagonally towards F<sub>2</sub>, passing under the strand C<sub>2</sub>-J<sub>2</sub> and over C<sub>1</sub>-J<sub>1</sub>.
#. Cross behind the back right under the strand E<sub>1</sub>-F<sub>1</sub> towards E<sub>2</sub>.
#. Cross diagonally in front of the left leg towards H<sub>2</sub>, passing under the strand D<sub>1</sub>-G<sub>1</sub>.
#. Pass through between the legs and cross behind the left leg towards G<sub>2</sub>.
#. On the front, cross diagonally towards D<sub>2</sub>, crossing under E<sub>2</sub>-H<sub>2</sub>, over E<sub>1</sub>-H<sub>1</sub>, over C<sub>2</sub>-J<sub>2</sub>, under C<sub>1</sub>-J<sub>1</sub>.
#. Pass to the back and go around the waist to C<sub>3</sub>.
#. On the front, go diagonally to J<sub>3</sub>, crossing under D<sub>1</sub>-G<sub>1</sub>, over D<sub>2</sub>-G<sub>2</sub>, over F<sub>1</sub>-I<sub>1</sub>, under F<sub>2</sub>-I<sub>2</sub>.
#. Cross behind the right leg towards I<sub>3</sub> and pass to the front between the legs.
#. On the front, cross diagonally towards F<sub>3</sub>, passing under C<sub>3</sub>-J<sub>3</sub>, over C<sub>2</sub>-J<sub>2</sub>, under C<sub>1</sub>-J<sub>1</sub>.
#. Cross behind the back right under the strand E<sub>2</sub>-F<sub>2</sub> towards E<sub>3</sub>.
#. Cross diagonally in front of the left leg towards H<sub>3</sub>, passing over D<sub>1</sub>-G<sub>1</sub> and under D<sub>2</sub>-G<sub>2</sub>.
#. Pass through between the legs and cross behind the left leg towards G<sub>3</sub>.
#. On the front, cross diagonally towards D<sub>3</sub>, crossing under E<sub>3</sub>-H<sub>3</sub>, over E<sub>2</sub>-H<sub>2</sub>, under E<sub>1</sub>-H<sub>1</sub>, over C<sub>3</sub>-J<sub>3</sub>, under C<sub>2</sub>-J<sub>2</sub>, over C<sub>1</sub>-J<sub>1</sub>.
#. You should now have a weave pattern as in the following figure.

	![Hip weave - front view.](front.png)

#. On the back, go horizontally to the center of the back right under the strand C<sub>2</sub>-D<sub>3</sub>, then cross upwards behind all strands on the waist.
#. Cross down in front of all strands on the waist, and then behind the strand coming from D<sub>3</sub>, to create a friction.
#. Cross down further behind all strands E-F.
#. Cross back up in front of all strands E-F, pulling them towards the upper horizontal strands and securing them against sliding down.
#. Split the rope ends, and pass them in opposite directions behind the vertical stem which connects the upper and lower horizontal lines.
#. Fix the ends with a half knot (ABOK #47). This will leave you with the following figure.

	![Hip weave - back view.](back.png)

#. If there is rope left, it is advised to tie another half knot (ABOK #47) in the rope ends, creating a reef knot (ABOK #1204) or granny knot (ABOK #1206).

## References

* <http://www.youtube.com/watch?v=r8Tttyji78s>
