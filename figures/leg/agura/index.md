# Agura

## Description

The Japanese term agura (&#x80e1;&#x5ea7; - cross-legged sitting) refers to a seated position with crossed legs. This tie reproduces this position. This is the basic variant; see [this version](../agususp/) for a variant suitable for suspension.

## Instructions

#. (to come)

## References

* <http://www.theduchy.com/agura-simple/>
