# Handcuffs

## Description

These instructions show different ways to tie handcuffs and how to apply them in a "belly chain" configuration. These are based on collapsing knots, which means that they can be tied loosely before being applied, and will get tighter as one pulls on them.

## Instructions

### Quick (Texas) handcuffs

These handcuffs are especially fast and easy to tie. Memorizing the steps, one can also quickly tie them "in the air" with both hands within seconds.

#. In the middle of a rope, create two loops in the same direction, as shown below.

	![Initial loops.](loops1.png)

#. Place the loops on top of each other, such that the lower side of one loop lies on top of the upper side of the other loop, as shown below.

	![Loops placed on top of each other.](loops2.png)

#. Pull the lower loop upwards through the upper loop and the upper loop downwards through the lower loop, forming a handcuff knot (ABOK #1140).
#. The two pulled loops can now be used as handcuffs as in the following picture.

	![Loops pulled through each other.](texas.png)

### Multi-loop handcuffs

The multi-loop handcuffs are a bit more visually appealing due to the additional loops around the central part. In contrast to the quick handcuffs shown before, they do not use the handcuff knot (ABOK #1140), but are obtained by adding more loops in the center of a Tom fool's knot (ABOK #1141).

#. Start my making several loops in the middle of a rope, as shown below.

	![Some loops in the middle of a rope.](mloop.png)

#. Grab through the loops from each end and pull a bight from each end through the loops, to form the figure below.

	![Multi-loop handcuffs.](multi.png)

### Prusik handcuffs

Another very suitable handcuff tie can be constructed from the Prusik knot or double strap (ABOK #1763). It is more stable and harder to escape that the previously shown ties, since pulling on the cuffs increases the friction, thus holding them in place.

#. Start by tying a Prusik knot (ABOK #1763) in the middle of a rope. It can be helpful to tie it around, e.g., a finger or other column.

	![A Prusik knot around a column.](ds.png)

#. Remove the column from the center and run each end of the rope through, as shown below.

	![Prusik knot handcuffs.](prusik.png)

#. Tighten first the loops which run around the moving ends, then adjust the cuff size.

### Applying handcuffs

Any of the previously shown knots can be used to create a pair of handcuffs, which can then be applied to someone's wrists and tightened. The following shows how to use them to tie the wrists to a "belly chain".

#. Place the forearms next to each other such such the inner sides of the hands will face each other.
#. Put the handcuffs over the wrists such that the knot from which the ropes come out will be away from the thumbs.

	![Handcuffs placed on wrists.](apply1.png)

#. Pull the handcuffs tight.
#. Fix the tension with an overhand knot (ABOK #47).

	![Handcuffs fixed with overhand knot.](apply2.png)

#. To create a "belly chain", bring the hands as close to the body as possible and pull the rope ends around the waist.
#. On the back, finish the tie with a ligature knot (surgeon's knot, ABOK #1209).

	!["Belly chain" tied with surgeon's knot.](apply3.png)

## References

* <http://www.theduchy.com/quick-cuffs/>
* <http://www.theduchy.com/multi-loop-handcuffs/>
* <http://www.theduchy.com/prusik-cuffs/>
* <http://www.theduchy.com/applying-handcuff-knots/>
