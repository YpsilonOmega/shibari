# Shibari figures

## About this project

The aim of this project is to create a repository of shibari figures, with drawings created using LaTeX / TikZ and instructions written in Markdown. The final figures are published as a website at <http://ypsilonomega.gitlab.io/shibari/>.

## How the figures are created

See [this webpage](http://tex-talk.net/2013/04/how-can-i-draw-a-knot-in-tex-let-me-count-the-ways/) for an introduction on how to draw knots using the method applied here. The syntax should mostly become clear from the files in this repository and is briefly summarized below.

### Tools used to create the pages

The following tools are used here to create the instructions and drawings:

* [LaTeX](http://www.latex-project.org/) - a scientific typesetting system.
* [TikZ](http://mirrors.ctan.org/graphics/pgf/base/doc/pgfmanual.pdf) - a powerful graphics library for LaTeX.
* [knots](http://mirrors.ctan.org/graphics/pgf/contrib/spath3/knots.pdf) - a TikZ package for drawing knots.
* [hobby](http://mirrors.ctan.org/graphics/pgf/contrib/hobby/hobby.pdf) - a TikZ package for drawing smooth Bézier curves.
* [ImageMagick](http://www.imagemagick.org/) - a versatile image manipulation suite.
* [Pandoc](http://pandoc.org/) - a universal text document converter.

### How to create figures

Take a look at this minimal example source file:

```tex
\documentclass{shibari}

\begin{document}
\begin{tikzpicture}
\begin{knot}
\strand (-2,-1) to +(3,0) arc (-90:0:0.5) to +(0,3);
\dstrand (0,-3) to +(0,3) arc (180:90:0.5) to +(3,0);
\end{knot}
\end{tikzpicture}
\end{document}
```

The following elements can be found here:

* `\documentclass{shibari}` loads the LaTeX class defined in the file `shibari.cls`, which is part of the repository. Note that if this file is not in the same folder as the source file, appropriate path specifiers must be prepended. It loads the LaTeX `standalone` class, which produces a standalone drawing as output. Further, it loads the TikZ library and knot drawing packages and sets a number of default options, such as line thickness and distance of double strands.
* `\begin{document}` &hellip; `\end{document}` enclose the document content.
* `\begin{tikzpicture}` &hellip; `\end{tikzpicture}` define a TikZ drawing.
* `\begin{knot}` &hellip; `\end{knot}` define a knot diagram and enable the special treatment of strand crossings.
* `strand` denotes the beginning of a single strand (single rope) in the diagram, followed by the starting coordinates.
* `dstrand` denotes the beginning of a double strand (double rope) in the diagram, as most often encountered in shibari, followed by the starting coordinates.
* Coordinates are entered in the form (x,y) for absolute coordinates or +(x,y) for relative coordinates.
* `to`, followed by coordinates, draws a straight line segment to the given coordinates.
* `arc`, followed by a triple (&alpha;:&beta;:r), draws an arc segment with starting angle &alpha;, ending angle &beta; and radius r. Angles are measured in degrees counterclockwise starting from the East (right) direction.

To compile the document, run:

	pdflatex example.tex

Finally, for the web pages we convert this PDF file to PNG:

	convert -flatten -density 120 example.pdf example.png

### How to create instruction sheets

Instruction sheets are written in Pandoc flavored Markdown. To include figures, note that they are converted to PNG for web viewing as shown above, and so the corresponding output file of the form `example.png` must be included. The Markdown is finally converted to HTML using this command:

	pandoc -c "/shibari/shibari.css" -T "`grep -Em1 '^#\s+.*$' example.md | sed -Ee 's/^#\s+//'`" -s -f markdown -t html5 -o example.html example.md
