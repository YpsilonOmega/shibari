# Mt. Fuji 3-Rope TK

## Description

A decorative possibility to add a third rope to an existing 2-rope TK is by creating a pattern in the front which resembles Mt. Fuji.

## Instructions

#. Start with the rope ends coming from the last kannuki O, and extend if necessary.
#. Cross upward over the two main chest wraps, by using square friction knots (ABOK #1173).
#. Cross behind the neck and over the shoulder A on the opposite side from the kannuki O you started from.
#. On the front side, go over the shoulder and back under the arm to C on the same side as you came from.
#. On the back, cross from C to F, going under the stem to the initial single column tie.
#. Go to the front under the arm F, crossing to the shoulder B on the same side.
#. On the back, use a crossing knot (ABOK #1173) to cross over the diagonal strand which goes to the other shoulder A.
#. Use two more crossing knots (ABOK #1173) to cross down over the two main chest wraps.
#. Go to the front under the arm D.
#. On the front, cross diagonally over the chest, and under the strand connecting B and F.
#. Reverse direction, to cross horizontally back over the strand B-F and further over the strand A-C.
#. Reverse direction again, going under the strand A-C.
#. Wrap the rope ends around the strand coming from D, to create a "wavy mountain top" pattern.
#. Go diagonally down and to the back under the arm E. The front should look as in the figure.

	![Front side view.](front.png)

#. On the back, fix the rope ends with another crossing knot (ABOK #1173) over the initial stem, including also the strand C-F, towards X.
#. The result should look as in the following figure.

	![Back side view.](back.png)

## References

* <http://www.theduchy.com/takate-kote/#mt-fuji-3tk>
