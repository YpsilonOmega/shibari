# X 3-Rope TK

## Description

A classical possibility to add a third rope to an existing 2-rope TK is by creating a X-shaped pattern in the front.

## Instructions

#. Start by either extending towards the rope ends of an already locked off TK, or by attaching another rope to the central stem on the back.
#. Go to the front under the arm C.
#. In the front, cross diagonally over the chest from C to the opposite shoulder B.
#. On the back, cross over the neck and then down with two crossing knots (ABOK #1173) over the two chest wraps.
#. Cross over the central stem that leads towards the initial single column tie.
#. Reverse direction and go back under the initial stem.
#. Reverse direction again and go once more over the stem.
#. Cross upwards over the two chest wraps using crossing knots (ABOK #1173).
#. Use another crossing knot (ABOK #1173) to cross diagonally over the neck rope towards the shoulder A.
#. On the front, cross again diagonally over the chest under the arm D on the opposite side. This creates the X pattern:

	![Front side view.](front.png)

#. On the back, attach the rope end with a half hitch (ABOK #48) to the initial stem towards X.
#. The back should follow the following picture:

	![Back side view.](back.png)

## References

* <http://www.theduchy.com/takate-kote/#3tk>
