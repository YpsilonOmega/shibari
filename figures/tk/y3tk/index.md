# Y 3-Rope TK

## Description

One possibility to add a third rope to an existing 2-rope TK is by creating a Y-shaped pattern in the front.

## Instructions

#. Start with the rope ends coming from the last kannuki O, and extend if necessary.
#. Pass under the stem leading to the single column tie of the hands, back over and under once again, to create a wrap.
#. Continue upwards, crossing under the two main chest wraps, and towards the shoulder A.
#. For the front, there are different possible options:

	#. From the shoulder A, go over the upper chest wrap, under the lower one, then back up again first over, then under, finally crossing the rope from A and going to the other shoulder B:

		![Y shape weaved into chest wraps.](front1.png)

	#. Instead of weaving into the chest wraps, stay over the upper wrap in both directions, and twist only around the lower wrap:

		![Y shape with single twist on lower chest wrap.](front2.png)

	#. Instead of making a single twist, twist the shoulder ropes again:

		![Y shape with multiple twists on lower chest wrap.](front3.png)

#. On the back, coming from the shoulder B, go under the upper chest wrap, over the lower one and the stem.
#. Continue upwards under the lower chest wrap, then above across the stem and the rope end coming from B.
#. Finally pull the rope down below the lower chest wrap towards X, creating an X-friction.
#. The back will yield the following picture:

	![Back side view.](back.png)

## References

* <http://www.theduchy.com/takate-kote/#y-3tk>
