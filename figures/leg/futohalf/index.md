# Half Hitch Futomomo

## Description

The half hitch futomomo is a decorative leg tie, which features a "braiding" pattern on one side.

## Instructions

#. Start with a single column tie on the ankle.
#. Bring the thigh and ankle close together and wrap on the inside towards the thigh A<sub>1</sub>.
#. Wrap around on the outer side of the leg towards B<sub>1</sub>.
#. Continue to wrap on the inner side towards A<sub>2</sub>.
#. Repeat the following pattern for each further wrap:
   #. Cross downwards over all previous wraps on the outer side of the leg.
   #. Cross back upwards behind all previous wraps.
   #. Cross over the current wrap.
   #. Continue wrapping, keeping the same direction.
   #. On the inner side of the leg, go around without any crossings.
#. Make sure that the loops are next to each other as on the picture (shown here for four wraps):

	![Outer side of the leg.](front.png)

#. Once you have reached the desired number n of wraps, cross downward on the inner side of the leg from B<sub>n</sub> over all wraps.
#. Cross back up behind all wraps.
#. Cross down again over all wraps except for the first (lowermost) one.
#. Cross back up behind all wraps except the first one.
#. Cross down over all wraps except the first one, and further down behind the first one.
#. Cross back up over the first wrap.
#. Pull the rope ends through the last loop created.
#. For extra safety, one may cross up over all wraps except the first one again, then back down behind them, and pull the rope ends through this final loop to X.

	![Inner side of the leg.](back.png)

## References

* <http://www.youtube.com/watch?v=bYZrpNGjdps>
