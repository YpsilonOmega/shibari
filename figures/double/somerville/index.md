# Somerville bowline double column

## Description

The [Somerville bowline](../../single/somerville/) can also be tied as a double column tie. The basic shape is very similar.

## Instructions

#. Start with two or three wraps of rope around the two column, leaving some space between them and about 30cm at the short end Y.
#. Create a loop in the long end X and lay it over the wraps.
#. Pull the short end through the loop from below.
#. Wrap the short end once around all wraps - first over the long ends, then pull through under the wraps which are located underneath the columns, hence passing through between the columns.
#. Pull the short ends once more through the loop, in the same direction as before.
#. Pull the short ends tighter, in order to pinch the wraps together, until you have reached the desired tension.
#. To fix the tension, wrap the short ends once more around the upper part of the wraps only, and again through the loop in the same direction.
#. Pull both ends X and Y tight. You should have this result now:

	![Somerville bowline double column](final.png)

## References

* <http://crash-restraint.com/ties/69>
