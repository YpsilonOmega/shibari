# Shinju for suspension

## Description

This is a stronger version of the [shinju](../shinju/) which is suitable for suspension. It doubles the number of shoulder wraps and has additional friction between the wraps in order to increase stability.

## Instructions

#. Place the center bight at the middle of the back.
#. Lay the rope around the upper part of the chest from A to B.
#. On the back, pull the rope through the center bight to form a lark's head (ABOK #5).
#. Lay the rope around the lower part of the chest from C to D.
#. Cross below the strand B-C and lay another wrap around the upper part of the chest from E to F, closely below the wrap A-B.
#. Cross above the strand D-E and lay another wrap around the lower part of the chest from G to H, closely below the wrap C-D.
#. From H, cross below the strands B-C and F-G. The back should now look as follows:

	![Suspension shinju - back view after horizontal wraps.](back.png)

#. Cross over the strands towards BE and AF, then under those towards AF, forming a half hitch (ABOK #48).
#. Pass the ropes to the front over the left shoulder at I.
#. In the front, cross downwards to the center of the chest over all horizontal strands A-B, E-F, C-D and G-H.
#. Cross back upwards under the strands G-H and C-D, coming out at the side closer to the left shoulder I.
#. Cross over the strand from the shoulder I, then under the strands E-F and A-B towards the right shoulder at J.
#. On the back, weave the rope by crossing under the strands towards BE, over CG, under DH and over AF, towards the left shoulder at K.
#. In the front, cross downwards to the center of the chest over all horizontal strands A-B, E-F, C-D and G-H, next to the strand coming from I.
#. Cross back upwards under the strands G-H and C-D, coming out at the side closer to the left shoulder IK.
#. Cross over the strand from the shoulder K, through the loop formed by the strands I-J and then over the strands E-F and A-B towards the right shoulder at L, so that the front looks as follows:

	![Suspension shinju - front view.](front.png)

#. On the back, cross over the strands towards BE, under those towards CG and over those to DH.
#. Cross back under the strands towards DH, forming a loop.
#. Lock the tie with a half hitch (ABOK #48) on the strands to DH, so that the result looks as follows:

	![Suspension shinju - final back view.](final.png)

## References

* <http://www.theduchy.com/suspension-shinju/>
* <http://crash-restraint.com/ties/130>
