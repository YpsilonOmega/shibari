# Barre harness

## Description

This is a purely decorative chest harness, which combines several horizontal stripes across the chest with vertical lines framing the outline of the chest. It does not use any locks, but only tension and friction to stay in position. Any excess rope left after tying this harness can be used to create a decorative spiral pattern.

## Instructions

#. Start with the center bight on the lower center of the back, and make a wrap around the narrowest part of the waist from A to B, pulling it tight enough so that it will stay in position.
#. Pull the rope ends through the center bight to create a lark's head (ABOK #5).
#. Reverse direction and create another wrap around the waist from C to D, keeping the tension to keep it in position.
#. On the back, pull the rope ends through the previous bight B-C and reverse direction again.
#. Make another wrap from E to F on the lower part of the chest. Make sure that all three wraps are equally spaced and tight.
#. On the back, pull the rope ends through the previous bight D-E and reverse direction again.
#. Make another wrap from G to H on the upper part of the chest.
#. On the back, pull the rope ends through the previous bight F-G, reverse direction and continue towards the left shoulder I. This will create the pattern in the following figure.

	![Back view before front wraps.](back0.png)

#. Go over the left shoulder to the front and cross down over the uppermost strand G-H.
#. Cross back up under the same strand G-H on the outer (left) side.
#. Cross horizontally over the vertical strand coming from the shoulder I.
#. On the other side, make a right angle and cross down behind the strand G-H.
#. Cross back up over the strand G-H and under the horizontal strand just created towards the right shoulder J. The pattern should resemble the figure below, which has the shape of a cow hitch (ABOK #1673).

	![Front view after first wrap.](front1.png)

#. On the back, cross over the strand that goes to the left shoulder I, then under the strand coming from H, over the vertical stem and under the strand going to G.
#. Continue towards the right shoulder K. The rope should follow the following figure.

	![Back view between front wraps.](back1.png)

#. On the front, cross down over the uppermost strand G-H, then back up behind it, and down in front of it again, wrapping towards the center of the chest.
#. Repeat the same crossing pattern for the next two strands E-F and C-D.
#. Cross down over the lowermost strand A-B and back up again behind it, but this time coming out on the outer (right) side.
#. Cross horizontally over the vertical strand towards the center.
#. On the other side, make a right angle and cross down behind the strand A-B.
#. Cross back up over the strand A-B and under the horizontal strand just created, forming another cow hitch (ABOK #1673) on the lowermost strand.
#. Cross up across each of the three remaining horizontal strands, mirroring the pattern on the other side.
#. Pass the rope ends to the back over the left shoulder L. The pattern should consist of horizontal and vertical lines as in the following figure.

	![Front view after second wrap.](front2.png)

#. Coming from the shoulder L, cross over the strand coming from H and under the vertical stem, as shown in the figure below.

	![Back view after front wraps.](back2.png)

#. If there is a large amount of excess rope, it can be used to create a spiral pattern, which alternately crosses above the below the radial strands G, H, I, J, K, L and the vertical stem.
#. A shorter amount of excess rope can be wrapped around the vertical stem. The complete pattern on the back is shown in the following figure.

	![Back view with used up excess rope.](back3.png)

## References

* <http://www.theduchy.com/barre-harness/>
