# Lace up armbinder

## Description

This tie is suitable for people with any shoulder flexibility. The advantage over other, similar ties is that it can be tightened later to any desired degree.

## Instructions

#. Start with a fixed point in the middle between the shoulders. For example, a [quick shoulder harness](../../chest/shoulder/) can be used, where the last half hitch (ABOK #48) can be omitted, since tension is maintained on the end X, as in the figure below.

	![Quick shoulder harness.](../../chest/shoulder/qsh.png)

#. Split the rope ends, and continue with a single rope towards each arm. Repeat the following steps for each arm.
#. Directly under the shoulder, cross over the arm, wrap around and come back under the armpit.
#. Cross over the horizontal line from the previous step, and then change direction to go downwards.
#. Go down along the arm for about 5-10cm, then pass to the front between the arm and the upper body.
#. Wrap around the arm and come back on top of it.
#. Cross under the vertical line coming from the previous wrap, change direction and go downwards.
#. Repeat the same pattern, making wraps in alternating directions around the arm, thereby moving further down. Do not place any wraps near the elbow.
#. After the last wrap, reverse direction around the arm to make a cow hitch (ABOK #1673).
#. Lock the tie with a half hitch (ABOK #48) around the vertical line connecting the last two wraps. This will lead to the following figure.

	![Wraps on both arms.](sides.png)

#. Take another rope, and place its center in the middle between the wrists.
#. Pass the rope ends through the lowermost vertical line on either side.
#. Cross the rope ends in the middle, and pass them through the next vertical line on the opposite side.
#. Repeat this for every vertical line, hence lacing up the tie. Adjust the tension as desired.
#. After the last wrap, lock the tie with a square knot (ABOK #1204) as in the following figure.

	![Laced up tie.](final.png)

#. Any excess rope can be wrapped around the original shoulder harness to tighten it up, as it might get lose during the lacing step.

## References

* <http://crash-restraint.com/ties/39>
