# U friction

## Description

This friction can be used, for example, in a takate kote. It essentially consists of two [L frictions](../fril/). It should be tied as tight and compact as possible.

## Instructions

#. Start with the rope coming from X parallel to the strand A-B, which is in front of the strand C-D.
#. Cross to the right under the rope coming from C.
#. Cross upwards on top of the rope coming from B.
#. Reverse direction and cross back down under the rope coming from B.
#. Cross to the left over the rope coming from C.
#. Cross upwards under the rope coming from A.
#. Reverse direction again and cross back down above the rope coming from A.
#. Cross once more to the right under the rope coming from C towards Y, creating the figure below.

	![Completed U friction.](friu.png)

## References

* <http://www.theduchy.com/frictions/>
