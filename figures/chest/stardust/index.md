# Stardust harness

## Description

The stardust harness is a decorative chest harness which resembles the shape of a pentagram.
## Instructions

#. Start at the center of the back and make a wrap around the lower part of the chest from A to B.
#. On the back, create a lark's head (ABOK #5) by pulling the ends through the center bight and reversing direction towards C.
#. Make another wrap around the lower part of the chest from C to D closely below the first one.
#. On the back, pull the rope ends through the bight BC and continue upwards to the left shoulder E.
#. In the front, cross downward over the two wraps AB and CD in the center of the chest.
#. Cross up again under the wraps AB and CD, then over the strand coming from E and towards the right shoulder F.
#. On the back, cross over the strand to E, then back under it, and over the strand coming from F towards the side G.
#. Go to the front under the arm at G and once around the strand to F.
#. Go back under the arm at H.
#. On the back, cross under the strand from F and over the strand from E towards I.
#. Go to the front under the arm at I and once around the strand to E.
#. Go back under the arm at J.
#. On the back, cross under the vertical stem and over all other strands towards the left shoulder K.
#. In the front, cross over the strand to F, then back under it and over the strand coming from K.
#. Continue counterclockwise to create a pentagon in the center of a pentagram, changing the order of crossing over and under at each corner.
#. Use a crossing knot (ABOK #1173) to cross over the strand from K towards the right shoulder L.
#. The front will now resemble the pentagram:

	![Front view.](front.png)

#. On the back, cross over the strands from K, E, I and under the strand from J.
#. Finish the tie with a half hitch (ABOK #48) on the vertical stem.
#. The back will resemble the following picture:

	![Back view.](back.png)

## References

* <http://www.theduchy.com/stardust-harness/>
