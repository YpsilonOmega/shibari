# Figure 8 inline double column

## Description

This tie can be used for attaching two parallel limbs to a stem running between them. It is useful in situations when a cuff is supposed to be made from the running ends of the rope, whose initial center bight is already attached somewhere else. There are different ways to finish it, depending on the direction towards which the running ends are supposed to continue.

## Instructions

#. Start with the running ends from the point X where they are attached and let them run in parallel with the limbs, down to where the edge of the cuff which is further away from X is supposed to be.
#. Make a right angle, and cross over the first limb.
#. Go behind the limb, and cross over the vertical rope coming from X, towards Y, as in the figure below.

	![First wrap.](first.png)

#. Continue by crossing over the second limb, and going behind it.
#. This time cross behind the vertical rope from X, above the first crossing, towards Y. This will create the figure below.

	![Second wrap.](second.png)

#. Continue with the same pattern:

	* cross over the first limb
	* go behind it
	* cross over the vertical line from X
	* cross over the second limb
	* go behind it
	* cross behind it the vertical line from X

	After three wraps, the pattern will be as follows:

	![Last wrap.](last.png)

#. Pull the rope ends through the wraps in parallel to the first limb towards Y, such that the crossings of the "figure 8" are centered between the limbs and between the two vertical lines, as in the figure below.

	![Pulling the rope ends on the other side.](through.png)

#. Pull the rope ends up again parallel to the second limb and the initial rope coming from X, towards Y in the figure below.

	![Finish with tension pulling up.](up.png)

#. Depending on how to continue with the running ends, there are different possibilities to finish this tie:

	#. If one continues upward holding the tension, then no further attachments are needed. Continue towards Y.
	#. If the tie continues downwards instead, pull the rope ends once more through the wraps on the first limb towards Y in the figure below.

		![Finish with tension pulling down.](down.png)

	#. If the tension is not maintained by further ties, it can be locked with a half hitch (ABOK #48) as in the next figure.

		![Finish with locked tension.](locked.png)

## References

* <http://crash-restraint.com/ties/255>
