# Square friction

## Description

The square friction is a particularly rigid friction to connect to rope strands which cross at a right angle. The rope crosses in an alternating pattern above and below these strands, forming a square shape. It should be tied as tight and compact as possible. Here two variants are shown, which are essentially identical and exchangeable - the end is chosen differently, depending on the direction in which the tie is supposed to continue.

## Instructions

### Variant 1

#. Start with the rope coming from X parallel to the strand A-B, which is behind the strand C-D.
#. Continue next to the strand A-B and cross under the rope coming from C.
#. Reverse direction and cross back to the right over the same strand coming from C.
#. Cross upwards under the strand coming from B.
#. Cross to the left over the strand coming from D.
#. Cross down under the strand coming from A.
#. Cross once more to the right over the strand coming from C.
#. Reverse direction and cross back to the left under the same strand coming from C.
#. Continue with the tie towards A. The result should resemble a square as follows.

	![Square friction - variant 1.](frisq1.png)

### Variant 2

#. Start with the rope coming from X parallel to the strand A-B, which is in front of the strand C-D.
#. Continue next to the strand A-B and cross over the rope coming from D.
#. Reverse direction and cross back to the right under the same strand coming from D.
#. Cross downwards over the strand coming from B.
#. Cross to the left under the strand coming from C.
#. Cross up over the strand coming from A.
#. Cross once more to the right under the strand coming from D.
#. Reverse direction and cross back to the left over the same strand coming from D.
#. Reverse direction once more and cross back to the right under the same strand coming from D.
#. Continue with the tie towards B. The result should resemble a square as follows.

	![Square friction - variant 2.](frisq2.png)

## References

* <http://www.theduchy.com/frictions/>
