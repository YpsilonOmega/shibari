# Drum harness

## Description

The drum harness is a decorative, load-bearing hip harness that can be used for suspension in different positions. The main feature is the "drum-like" connecting pattern between the waist and thigh loops. There are different variants to make this tie.

## Instructions

### Older version

#. Start with a lark's head (ABOK #5) around the waist from. Make sure that the rope is on the upper part of the hip bone, not on the soft part of the abdomen.
#. Reverse the direction and make another wrap around the waist just above the previous one.
#. Run the rope ends through the bight between the two wraps, where you reversed direction.
#. Lock the tension with a half hitch (ABOK #48) around the wraps.
#. Pass the rope through between the legs, on the side in which you started tying with the initial lark's head, and make a wrap around the thigh, passing between the thigh and butt muscles.
#. Make another wrap in the same direction, just below the previous one.
#. After finishing the second wrap, cross over both wraps and back the same way under those wraps.
#. Turn back and cross over the wrap you just finished, then under the one above, continuing around the hip.
#. Cross upward over the waist wraps, then downward below them, and finally over the connecting strand you have just created, further following the same direction around the hips.
#. Cross downward over the leg wraps, then upward below then, and over the connecting strand, to mirror the loop you created on the waist wraps.
#. Continue the same pattern around the hips, adding a loop on the waist wraps, one on the leg wraps and finally another one on the waist wraps, that should be in the center of the back.
#. Pass the ropes around the other leg, first on the outside towards the front, then back between the legs.
#. Make another wrap around the leg in the same direction, just below the previous one, so that they will be at the same height as on the first leg.
#. After finishing the second wrap, cross upward over both wraps, then downward below them.
#. Turn back towards the inner side of the thigh and cross upward again over the two leg wraps.
#. Cross below the connecting strands, forming a crossing knot (ABOK #1173), to continue in the same direction around the hips.
#. Continue the drum patter by making a loop on the waist wrap, second leg wrap, waist wrap, second leg wrap and waist wrap again, in the same way as for the first leg, so that the last loop will be in front of the second leg.
#. To create a stem in the front, wrap any excess rope back and forth around the two strands in the front which connect the waist and leg wraps, always passing first below and then above.
#. Wrap the remaining excess rope around the stem and tuck the ends inside towards X.
#. The complete harness will resemble the following picture (seen from below):

	![Finished drum harness (older version).](old.png)

### Newer version

#. Start with the steps 1 to 4 of the older version of the harness.
#. Coming from the waist wraps, continue around the hips, then between the thigh and butt muscles, passing through the legs to the front, to create a wrap around the leg.
#. Make another wrap around the leg just below the previous one.
#. After finishing the second wrap, cross upward over both wraps, then downward below them.
#. Turn back towards the inner side of the thigh and cross upward again over the two leg wraps.
#. Cross below the connecting strands, forming a crossing knot (ABOK #1173), to continue around the hips on the same side where you made the leg wrap.
#. Continue with steps 9 to 19 of the older version.
#. The complete harness will resemble the following picture (seen from below):

	![Finished drum harness (newer version).](new.png)

## References

* <http://www.theduchy.com/drum-harness/>
* <http://www.youtube.com/watch?v=ER-BGkneZq4>
