# Agura (suspension)

## Description

This agura (&#x80e1;&#x5ea7; - cross-legged sitting) variant is designed for suspension. See [this version](../agura/) for the basic (non-suspension) pattern.

## Instructions

#. (to come)

## References

* <http://www.theduchy.com/agura-suspension/>
