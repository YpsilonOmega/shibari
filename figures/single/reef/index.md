# Reef knot single column tie

## Description

The reef knot single column tie is very similar to its [close relative based on the granny knot](../granny/). It might be a bit more stable, depending on the direction of the pulling force on the long end of the rope, and can be untied as easily. In general, it can be untied more easily that the [Somerville bowline](../somerville/). It is based on the reef knot (ABOK #1204).

## Instructions

#. Start with (at least) two wraps of rope around the column, leaving about 20cm at the short end Y.
#. Wrap the short end once around the wraps - first over the long ends, then pull through under the wraps.
#. Create a loop in the long ends X, which points *in the opposite direction* as the loop created by the short end Y.
#. Pull the shorts end Y from above through the loop in the long ends X.
#. Pull both ends X and Y tight. You should have this result now:

	![Reef knot single column tie](final.png)
