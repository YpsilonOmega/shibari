# CHC Harness

## Description

This is a decorative chest harness. In contrast to most shibari figures, it is tied with two ropes simultaneously, and the ends of the ropes initially follow different directions starting from their center, instead of continuing in the same direction from the center bight.

## Instructions

#. Take two ropes of equal length besides each other and start with their centers in the center of the back.
#. Lay the ropes horizontally across the back, with one pair of ends towards A and the other one towards B.
#. Wrap the ends around the lower part of the chest, with the wrap from A to D directly under that from B to C.
#. In the center of the back, cross the strands coming from C down over those coming from BD, and then back up behind them.
#. Cross the strands coming from D upward over the strands coming from AC.
#. Continue with all four ends next to each other towards the right shoulder E.
#. On the front, pass each rope end down behind the strands AC-BD, then back up in front of AC-BD, and towards the left side of the body behind themselves.
#. Cross all strands from the right side of the body over the existing strands towards the left shoulder F.
#. On the back, cross over the strands from E towards the right side.
#. Cross down under the strands from BD, then back up over them.
#. Cross behind all vertical strands towards the left side G.
#. Continue under the left arm straight from G to H.
#. With each strand coming from H<sub>n</sub>, make a cow hitch (ABOK #1673) over the outermost n strands coming from the left shoulder F, by first crossing over towards the center of the body, then back under, upwards over the strand from H<sub>n</sub>, then crossing under towards the center, back over and finally under the bight towards I<sub>n</sub>.
#. On the left side under the arm, cross all strands coming from I over the strands G-H towards J, as shown on the following picture:

	![Left side.](left.png)

#. On the back, cross horizontally over the strands from F and under those from E towards the right side K.
#. Continue under the right arm straight from K to L.
#. With each strand coming from L<sub>n</sub>, make a cow hitch (ABOK #1673) over the outermost n strands coming from the right shoulder E, by first crossing over towards the center of the body, then back under, downwards over the strand from L<sub>n</sub>, then crossing under towards the center, back over and finally under the bight towards M<sub>n</sub>.
#. The front will now resemble the following picture:

	![Front view.](front.png)

#. On the right side under the arm, cross all strands coming from M over the strands K-L towards N, as shown on the following picture:

	![Right side.](right.png)

#. On the back, cross horizontally under all vertical strands to the left, then back over them to the right.
#. Cross down behind the strands BD towards X to finish the tie.
#. The back will now resemble the following picture:

	![Back view.](back.png)

## References

* <http://www.theduchy.com/chc-harness/>
