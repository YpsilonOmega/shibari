# Munenawa

## Description

The munenawa (Japanese &#x80f8;&#x7e04; - chest rope) is a simple, classical, Japanese chest tie.

## Instructions

#. Start at the center of the back and make a wrap around the lower part of the chest from A to B.
#. On the back, create a lark's head (ABOK #5) by pulling the ends through the center bight and reversing direction towards C.
#. Make a wrap around the upper part of the chest from C to D.
#. On the back, pull the rope ends through the bight BC and upwards towards the left shoulder E.
#. In the front, cross downward over the strands C-D and A-B, then back up under A-B, over the strand coming from E and over the strand C-D towards the right shoulder F.
#. On the back, pass over the strand from E, under the strand from D and lock the tension with a half hitch (ABOK #48) on the bight B-C.
#. Split the rope ends and pass one end to each shoulder G<sub>1,2</sub>.
#. Repeat the following steps for each side:
   #. Pass downward under the strands A-B and C-D.
   #. Pass back up over A-B and C-D.
   #. Pass the rope inward under the strands coming from G<sub>1,2</sub> and E/F.
   #. Pass back outward over E/F and under G<sub>1,2</sub>.
   #. Pass inward again over both strands from G<sub>1,2</sub> and E/F.
   #. Pass back outward under E/F and over G<sub>1,2</sub> towards H<sub>1,2</sub>.
#. The front should resemble the following picture:

	![Front view.](front.png)

#. On the back, pass the rope from H<sub>1</sub> below the vertical lines to the right to meet with the rope from H<sub>2</sub>.
#. Cross over all vertical / diagonal strands to the left.
#. Use up any excess rope by repeatedly crossing under G<sub>1</sub> and E to the right, over F and G<sub>2</sub> to the right, under G<sub>2</sub> and F to the left and over E and G<sub>1</sub> to the left.
#. The back shows the following picture:

	![Back view.](back.png)

## References

* <http://www.theduchy.com/munenawa/>
