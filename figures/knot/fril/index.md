# L friction

## Description

The L friction is mostly useful to join some ropes in such as way that it prevents one rope from sliding into a particular direction, by setting up a barrier on the corresponding side. It can be tied in different ways, possibly reversing direction as well.

## Instructions

### Variant 1

#. Start with the rope coming from X parallel to the strand A-B, which is behind the strand C-D.
#. Cross to the left over the rope coming from D.
#. Cross down under the strand coming from A.
#. Reverse direction and cross back up over the same strand coming from A.
#. Cross to the right under the strand coming from D.
#. Reverse direction again and cross back to the left over the same strand coming from D.
#. If you continue with the tie towards A, the result should resemble the following figure.

	![L friction - variant 1 without reversing direction.](fril1a.png)

#. Alternatively, if you continue with the tie towards B, reverse direction once more and cross to the right under the strand coming from D. The result should then resemble the following figure.

	![L friction - variant 1 with reversing direction.](fril1b.png)

### Variant 2

#. Start with the rope coming from X parallel to the strand A-B, which is behind the strand C-D.
#. Follow the strand A-B and cross to the left under the rope coming from D.
#. Reverse direction and cross back to the right over the same strand coming from D.
#. Cross down under the strand coming from B.
#. Reverse direction again and cross back up over the same strand coming from B.
#. Cross to the left under the strand coming from D.
#. Continue with the tie towards A. The result should resemble the following figure.

	![L friction - variant 2.](fril2.png)

## References

* <http://www.theduchy.com/frictions/>
