# Prosperity knot belt

## Description

This belt features a simple, yet decorative knot, known as the prosperity knot (ABOK #2242).

## Instructions

#. Start by tying a double coin knot (ABOK #1428). The distance from the center bight will determine the distance of the prosperity knot once it is finished. It should look as in the following picture.

	![Initial double coin knot.](step1.png)

#. Widen the two loops of the double coin knot by pulling a part of the free ends back, as shown in the following picture.

	![Double coin knot with widened loops.](step2.png)

#. Twist each of the widened loops, in such a direction that crossing over and under strictly alternates when you follow the rope.

	![Twisting the widened loops.](step3.png)

#. Of the two free rope ends, take the one that comes out from below, and cross it over the twisted loop on the same side.

	![Crossing one free end over the twisted loop.](step4.png)

#. Take the other twisted loop, pull it from below through the first twisted loop, over the crossing rope end and back down through the loop.

	![Pulling the other twisted loop through the first one and back.](step5.png)

#. Take the remaining free rope end and weave it by going from below through the second twisted loop, then crossing in an alternating pattern over and under the strands shown in the picture. This will create the prosperity knot (ABOK #2242).

	![Final view of the prosperity knot.](buckle.png)

#. To attach the final rope belt, you can use a lark's head (ABOK #5) together with a half hitch (ABOK #48).

	![How to attach the rope belt.](attach.png)

## References

* <http://www.knottyboys.com/videos/Decorative_Ideas/Belt_Buckle.wmv>
