# Swiss seat

## Description

This tie is a quick and simple building block for a seated suspension. An upline is usually attached in the front center of the tie, so that the tie will provide a stable seat, whence its name.

## Instructions

#. Start with a lark's head (ABOK #5) around the waist from A to B. Make sure that the rope is on the upper part of the hip bone, not on the soft part of the abdomen.
#. Reverse the direction and make another wrap around the waist just above the previous one from C to D.
#. Run the rope ends through the bight between B and C.
#. Lock the tension with a half hitch around the strand to B and C, as shown below.

	![Waist belt.](upper.png)

#. Cross the rope over the left hip E, around between the thigh and butt muscles, and to the front again between the legs at F.
#. Pass the rope upward over the strands towards A-D-E.
#. Cross downward behind the strands, coming out in the acute angle between the strands to B-C and F.
#. Cross downward over the strand towards F.
#. The result is shown in the following figure. Make sure that the loop is tight and intersects the strand from B-C to A-D approximately at the left hip joint.

	![First wrap around left leg.](left1.png)

#. Cross once again over the left hip and around the thigh from G to H directly under the previous wrap, then to the front between the legs.
#. Pass the rope upward over the strands towards A-D-E-G.
#. Cross downward behind the strands, coming out in the acute angle between the strands to B-C and F-H.
#. Cross upward over the strands, and down again behind the strands to A-D, to form a half hitch (ABOK #48) as shown below.

	![After wraps around left leg.](left2.png)

#. Continue with the right hip, crossing at I, go around between the thigh and butt muscles, and to the front again at J.
#. Pass the rope upward over the strands towards B-C-I.
#. Cross downward behind the strands, coming out in the acute angle between the strands to B-C and J.
#. Cross downward over the strand towards J and pull tight.
#. Make sure the loop just formed is close to the right hip joint. The result is shown in the following figure. Note that the steps 14-18 are identical to the steps 5-9 for the left side.

	![First wrap around right leg.](right1.png)

#. Cross once again over the right hip and around the thigh from K to L directly under the previous wrap, then to the front between the legs.
#. Pass the rope upward over the strands towards B-C-I-K.
#. Cross downward behind the strands, coming out in the acute angle between the strands to B-C and J-L.
#. Cross upward over the strands, and down again behind the strands to B-C, to form a half hitch (ABOK #48).
#. Make another half hitch (ABOK #48) over the strand to B-C to safely lock the tie, as shown below.

	![After wraps around right leg.](right2.png)

#. Any excess rope can be used up by creating a stronger handle, first passing in front of all ropes towards the left leg straps.
#. After crossing over the leg straps, cross back below the left and right leg straps towards the right hip.
#. Cross upward over the strand to B-C.
#. The remaining rope can be wrapped around the central part and locked off with another half hitch (ABOK #48).

	![Finish with center handle.](final.png)

#. Check that on the back the rope is properly parallel and tightly fits around the pelvis.

	![Back view.](back.png)

## References

* <http://www.theduchy.com/swiss-seat/>
* <http://crash-restraint.com/ties/129>
