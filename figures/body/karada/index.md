# Karada

## Description

The karada (from Japanese &#x4f53; - body) is a body harness, which creates a simple, symmetric pattern on the front of the body. Different variations exist both for the front and back part of the tie. The most common forms of front designs are hishi (from Japanese &#x83f1; or &#x83f1;&#x5f62; - diamond shape, rhombus) and kikkou (from Japanese &#x4e80;&#x7532; - tortoise shell).

## Instructions

The karada can be varied in several places, changing either the front pattern, the back ties or the finishing tie-off. The general scheme is as follows:

#. Start by laying the center bight around the back of the neck, such that the free rope ends pass over the shoulders A and B to the front of the body.
#. Bring the ropes together in the front and down the body.
#. Prepare the front pattern. Most patterns are based on connecting the rope in the front by making several overhand knots (ABOK #46), starting from above the chest / upper end of the sternum, down to the hips. (As a decorative alternative, one may also use double coin knots (ABOK #1428) instead.)
#. Pass the rope ends between the legs C to the back. Make sure that the rope is not too tight, as it will be tightened further during the process.
#. Use one of the back tying schemes to bring the ropes repeatedly towards the front and back along the sides D<sub>1</sub>, E<sub>1</sub>, F<sub>1</sub>, G<sub>1</sub> to D<sub>n</sub>, E<sub>n</sub>, F<sub>n</sub>, G<sub>n</sub>.
#. Finish the figure by tying off the ends.

### Hishi style front

#. To prepare the front pattern, place knots in the rope that comes from the neck in a distance of 10-15cm.
#. When weaving the pattern by going to the front and back, pass once on each side through every loop between two knots.

![Hishi style karada front.](hishi.png)

### Kikkou style front

#. To prepare the front pattern, place knots in the rope that comes from the neck in a distance of 15-20cm.
#. When weaving the pattern by going to the front and back, pass twice on each side through every loop between two knots.

![Kikkou style karada front.](kikkou.png)

### Hishi style front without knots

An alternative hishi style front can also be achieved without making knots in the vertical front ropes. In this case the horizontal ropes are passed around the vertical ones in alternating order, such that at every level, the two vertical lines are pulled towards opposite sides, and the sides alternate at each level, creating a diamond shape pattern. This variant is faster to tie, but less stable if worn over longer time, compared to the knot variant.

![Alternative hishi style karada without knots.](hishi2.png)

### Twisted horizontal lines

Instead of simply passing from the back to the front, through a central loop and back again, one can also add a "twist" on the front in the horizontal lines, such that the one which continues upwards on the back crosses that one which continues downwards. This adds additional friction and stability to the pattern. Make sure that every horizontal line alternates between crossing over and under another rope, as shown in the figure.

![Twisted horizontal lines](twist.png)

### Back with central stem

The most common variant for tying the back uses a vertical stem. It helps to stabilize the tension of the vertical ropes.

#. Coming from between the legs C, go up along the back.
#. Slightly widen the loop A-B around the neck, pulling the center of the rope down to create some distance from the neck.
#. Run the rope ends through this loop.
#. Split the rope ends and run them to the front under the arm along D<sub>1</sub> and F<sub>1</sub>.
#. In the front, use the rope ends to create the first element of the pattern, then run them to the back under the arms at E<sub>1</sub> and G<sub>1</sub>.
#. On the back, cross the rope ends from E<sub>1</sub> to F<sub>2</sub> and from G<sub>1</sub> to D<sub>2</sub> above the vertical stem.
#. Repeat the front part as many times as needed for the front pattern.
#. After the final front loop, cross the rope ends coming from E<sub>n</sub> and G<sub>n</sub> behind the vertical stem.
#. Finish the tie by making a square knot (ABOK #1204) in front of the stem.

![Back with central stem.](backtop.png)

### Back with half hitch

Instead of just passing the ropes from the vertical stem through the loop A-B, one can also attach them with a knot, such as a half hitch (ABOK #48), before splitting them and continuing towards the front D<sub>1</sub> and F<sub>1</sub>. This additional knot fixes the tension of the vertical ropes.

![Back with half hitch.](backhh.png)

### Adding friction to horizontal lines

Similarly to the front, one may also add more friction to the horizontal lines on the back. One possibility is to simply twist the horizontal lines once more instead of simply crossing them:

![Twisting horizontal lines over the stem.](twists.png)

A more sophisticated pattern is obtained by weaving the horizontal lines through the stem:

![Weaving the stem and the horizontal lines.](weave.png)

Even without additional twists friction can be added by passing the stem on alternating sides:

![Crossing opposite side of the stem.](center.png)

### Back without central stem

If one is short in rope, one can also omit the vertical stem in the back, and tie the horizontal lines in the opposite order. This version fixes the tension only after the last knot, and so it is important to manually hold the tension while tying.

#. From between the legs C, split the ropes and go to the front over the hips E<sub>n</sub> and G<sub>n</sub>.
#. Create the front pattern from bottom to top, by repeatedly going to the front and back, crossing the rope ends in the back.
#. After the final pattern element coming from underneath the arms D<sub>1</sub> and F<sub>1</sub>, widen the loop A-B on the neck and pass the rope ends through.
#. Finish the tie by attaching the rope ends to one of the horizontal crossing lines on the back, using a half hitch (ABOK #48).

![Back without central stem.](backbot.png)

### Separated neck loop

Instead of simply passing through the loop A-B on the neck, one may also start the karada with an additional overhand knot (ABOK #46) close to the center bight, which separates a small loop that will be used later for passing through the back ropes. Make sure to pull this extra knot away from the neck, to avoid pressure on the neck.

![Separated neck loop](loop.png)

### Front tie-off

Another variant that is useful if the rope is too short to pass to the back after the final pattern element is to attach the ends to the final loop in the front, using half hitches (ABOK #48) or similar knots.

![Front tie-off.](frontfin.png)

## References

* <http://crash-restraint.com/ties/32> (hishi karada)
* <http://crash-restraint.com/ties/33> (knotless hishi karada)
* <http://www.theduchy.com/karada/> (hishi karada with alternative back)
