# Bikini harness

## Description

The front of the bikini harness is similar to the [Shinju](../shinju/), so it creates a similar visual effect, but the back and the order in which the parts are tied are quite different.

## Instructions

#. Start with the center bight in the center of the back.
#. Lay the ropes around the upper part of the chest from F to E.
#. Pull the rope ends through the center bight and continue towards C.
#. Lay the ropes around the upper part of the chest from C to D, closely over the strand E-F.
#. Pull the rope ends through the bight C-E and continue towards H.
#. Lay the ropes around the lower part of the chest from H to G.
#. Pull the rope ends through the center bight and continue towards I.
#. Lay the ropes around the lower part of the chest from I to J, closely under the strand G-H.
#. Pull the rope ends through both bights G-I and C-E.
#. Bring the rope over the left shoulder B to the front.
#. On the front, go over all strands C-D, E-F, G-H, I-J to the center of the lower part of the chest.
#. Return the rope under all strands I-J, G-H, E-F, C-D, cross over the strand from B and continue to the right shoulder A.
#. On the back, go over the strand from B, through the center bight, under the strands D-H and B-J, over the strand G-I and under the strands C-E and those towards A and B, so that the ends point towards the left shoulder.
#. Make another loop around the strands going to A and B, first over than under, and pull the rope ends through the loop. The result should look like this:

	![Bikini harness - front view.](front.png)

	![Bikini harness - back view.](back.png)

## References

* <http://www.theduchy.com/bikini-harness/>
