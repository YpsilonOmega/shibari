# Hair corset

## Description

This hair corset is a simple, decorative tie to wrap around a pony tail.

## Instructions

#. Start by tying the hair to a pony tail. Braiding is also a possible alternative.
#. Use the center bight of the rope, lay the rope ends once around the pony tail at the upper end from 1 to 1 and pull the ends from below through the center bight, forming a lark's head (ABOK #5).
#. Reverse direction and make another wrap from 2 to 2.
#. Pull the rope ends through the bight 1-2 from below.
#. Reverse direction again and make another wrap from 3 to 3.
#. Continue the same pattern, making further wraps 4 &hellip; M.
#. After the last wrap from N to N, when the desired length is reached or the rope is used up, pull the rope ends through the final bight M-N to X and pull tight.
#. The result should follow the following pattern.

	![Final view of the hair corset.](final.png)

## References

* <http://www.knottyboys.com/videos/Decorative_Ideas/Hair_Corset.wmv>
