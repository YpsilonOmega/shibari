# Futomomo for suspension

## Description

While the regular [futomomo](../futomomo/) can in principle also be used for suspension, this version is recommended for additional safety and stability. It uses double wraps instead of single wraps, and each pair of wraps is in addition stabilized using an [X friction](../../knot/frix/).

## Instructions

#. Bring the ankle as close to the thigh as possible, in order for the tie to become tight enough, and to keep it from getting loose later.
#. Start with a single column tie above the ankle. This should not be too tight and feel comfortable.
#. Wrap the rope once around the leg via A<sub>1</sub> and B<sub>1</sub>.
#. On the front, cross over the previous wrap, and make another wrap closely below it via C<sub>1</sub> and D<sub>1</sub>.
#. After the second wrap, cross once again over the strand coming from the single column tie.
#. Cross upward underneath the two wraps.
#. Cross diagonally down over the two wraps towards the ankle.
#. Cross up again under the two wraps.
#. Cross diagonally again over the two wraps, now down and towards the thigh.
#. Cross once more upward underneath the two wraps.
#. Repeat the previous steps 3-10 by creating two more wraps A<sub>2</sub>-B<sub>2</sub>-C<sub>2</sub>-D<sub>2</sub> and fixing them with another X friction.
#. Repeat the previous steps 3-9 by creating two more wraps A<sub>3</sub>-B<sub>3</sub>-C<sub>3</sub>-D<sub>3</sub> and fixing them with another X friction.
#. Pass the rope down towards X while wrapping it around those strands which connect pairs of wraps. The result should resemble the following figure:

	![Front view.](front.png)

#. On the other side of the leg, pass the rope over each pair of wraps with a crossing knot (ABOK #1173).
#. After crossing over the last wrap, tuck the rope ends under the horizontal part of the crossing know, back where the rope came from, thus forming a cow hitch (ABOK #1673). This will look as in the figure below.

	![Back view.](back.png)

## References

* <http://www.theduchy.com/suspension-futomomo/>
