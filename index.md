# Shibari figures

## About this page

This website is a repository of shibari figures, with instruction sheets written in Markdown and drawings made with LaTeX + TikZ. It is under continuous development.

NB! Keep in mind that rope bondage is *dangerous* and should *never* be practiced without sufficient knowledge about rope safety and possible risks. This page is *not* intended to be a stand-alone course for rope beginners. It is meant purely as a collection of figures to learn, and assumes that these are practiced only with the necessary knowledge and experience in rope bondage. Also keep in mind that the rope figures listed here may contain errors. If you tie them, use your own common sense, and be aware of possible risks!

NB! Blue links are internal links to other pages on this website, and all materials on this site are SFW drawings. Green links are external links and may contain explicit images or videos, and are hence potentially *NSFW*.

The instruction sheets make frequent reference to knots listed in [The Ashley Book Of Knots](http://en.wikipedia.org/wiki/The_Ashley_Book_of_Knots) (marked with "ABOK" and the number of the knot). It is recommended as additional reference material.

> For several reasons drawings are used here for illustrations instead of photographs, the most important reason of course being that since I am an artist this is my usual method of expression. But drawings also have certain definite advantages over photographs. There need never be any doubt, in a drawing, as to which is the end of a rope and which is the standing part. In the photograph of an actual knot, the standing part appears cut off as well as the end, so that often the two cannot be told apart.
>
> There need be no question in a drawing as to which strand is under and which is over at any point. In a photograph this is frequently obscured by shadow.
>
> Anything may be omitted in a drawing that is not required, such as the individual strands and yarns of a rope. These are of importance in depicting Multi-Strand Knots but are superfluous and frequently confusing in the illustration of Single-Strand Knots.
>
> -- <cite>Clifford W. Ashley, The Ashley Book of Knots</cite>

## Symbols

* &#x1f589; - instructions with drawings
* &#x1f5bb; - link to picture(s)
* &#x1f5ba; - link to tutorial with pictures
* &#x1f39e; - link to video tutorial

## Figures

### Helpful knots and techniques

* [Extending rope: lark's head to square knot method](figures/knot/extsquare/) &#x1f589; &#x1f5ba;
* [Extending rope: sheet bend method](figures/knot/extsheet/) &#x1f589; &#x1f39e;
* [X friction](figures/knot/frix/) &#x1f589; &#x1f5ba;
* [L friction](figures/knot/fril/) &#x1f589; &#x1f5ba;
* [U friction](figures/knot/friu/) &#x1f589; &#x1f5ba;
* [Square friction](figures/knot/frisq/) &#x1f589; &#x1f5ba;

### Single column ties

* [Lark's head single column tie](figures/single/lark/) &#x1f589; &#x1f39e;
* [Lark's head single column tie with extra wraps](figures/single/larkx/) &#x1f589; &#x1f5ba;
* [Somerville bowline](figures/single/somerville/) &#x1f589; &#x1f39e;
* [Reef knot single column tie](figures/single/reef/) &#x1f589;
* [Granny knot single column tie](figures/single/granny/) &#x1f589;
* [Slipped overhand cuff](figures/single/slipover/) &#x1f589; &#x1f5ba;
* [Captured overhand cuff](figures/single/captover/) &#x1f589; &#x1f39e;
* [Captured overhand gunslinger](figures/single/cogunslinger/) &#x1f589; &#x1f39e;

### Double column ties

* [Lark's head double column tie](figures/double/lark/) &#x1f589; &#x1f5ba; &#x1f39e;
* [Somerville bowline double column](figures/double/somerville/) &#x1f589; &#x1f39e;
* [Figure 8 inline double column](figures/double/figure8/) &#x1f589; &#x1f39e;

### Shoulder / arm / hand ties

* [Handcuffs](figures/arm/handcuff/) &#x1f589; &#x1f5ba;
* [Wrists behind neck](figures/arm/wbn/) &#x1f589; &#x1f39e;
* [Basic armbinder](figures/arm/basic/) &#x1f589; &#x1f39e;
* [Extra strict armbinder](figures/arm/strict/) &#x1f589; &#x1f39e;
* [Lace up armbinder](figures/arm/lace/) &#x1f589; &#x1f39e;
* [TK-style armbinder](figures/arm/tkstyle/) &#x1f5ba;
* [Dragonfly sleeve](figures/arm/dragonfly/) &#x1f589; &#x1f39e;

### Hip / leg / foot ties

* [Gravity boot](figures/leg/gravboot/) &#x1f589; &#x1f5ba;
* [Futomomo](figures/leg/futomomo/) &#x1f589; &#x1f5ba;
* [Half Hitch Futomomo](figures/leg/futohalf/) &#x1f589; &#x1f39e;
* [Futomomo (X friction, for suspension)](figures/leg/futosusp/) &#x1f589; &#x1f5ba;
* [Hip weave](figures/leg/hipweave/) &#x1f589; &#x1f39e;
* [Swiss seat](figures/leg/swiss/) &#x1f589; &#x1f5ba; &#x1f39e;
* [Drum harness](figures/leg/drum/) &#x1f589; &#x1f5ba; &#x1f39e;
* [Agura](figures/leg/agura/) &#x1f5ba; &#x1f39e;
* [Agura (suspension)](figures/leg/agura/) &#x1f5ba;

### Chest harnesses

* [Pentagram harness](figures/chest/penta/) &#x1f589; &#x1f5ba; &#x1f39e;
* [Stardust harness](figures/chest/stardust/) &#x1f589; &#x1f5ba;
* [Munenawa](figures/chest/munenawa/) &#x1f589; &#x1f5ba;
* [Shinju](figures/chest/shinju/) &#x1f589; &#x1f5bb; &#x1f39e;
* [Shinju for suspension](figures/chest/shinsusp/) &#x1f589; &#x1f5ba; &#x1f39e;
* [Bikini harness](figures/chest/bikini/) &#x1f589; &#x1f5ba; &#x1f39e;
* [Intimate chest harness](figures/chest/intimate/) &#x1f589; &#x1f39e;
* [Quick shoulder harness](figures/chest/shoulder/) &#x1f589; &#x1f39e;
* [Karada box tie](figures/chest/karadabox/) &#x1f589; &#x1f5ba;
* [Corselet harness](figures/chest/corselet/) &#x1f589; &#x1f39e;
* [Under-ribs karada](figures/chest/urkarada/) &#x1f589; &#x1f39e;
* [Barre harness](figures/chest/barre/) &#x1f589; &#x1f5ba;
* [CHC harness](figures/chest/chc/) &#x1f589; &#x1f5ba;

### Body harnesses

* [Karada](figures/body/karada/) &#x1f589; &#x1f5ba; &#x1f39e;
* [Star back harness](figures/body/starback/) &#x1f589; &#x1f39e;

### Takate Kote

* [Basic TK with only lower kannukis](figures/tk/basic/) &#x1f589;
* [2-Rope Gote TK](figures/tk/gote2tk/) &#x1f589; &#x1f5ba;
* [X 3-Rope TK](figures/tk/x3tk/) &#x1f589; &#x1f5ba;
* [Y 3-Rope TK](figures/tk/y3tk/) &#x1f589; &#x1f5ba;
* [Mt. Fuji 3-Rope TK](figures/tk/mtfuji/) &#x1f589; &#x1f5ba;
* [Curves ahead TK](figures/tk/curvesahead/) &#x1f589; &#x1f5ba;

### Rope fashion

* [Prosperity knot belt](figures/fashion/belt/) &#x1f589; &#x1f39e;
* [Hair corset](figures/fashion/hair/) &#x1f589; &#x1f39e;
* [Rigger gauntlet](figures/fashion/riggaunt/) &#x1f589; &#x1f39e;
* [Continuous reverse gauntlet](figures/fashion/contrevgaunt/) &#x1f589; &#x1f5ba;
* [Twisted 8 tube top](figures/fashion/t8tubetop/) &#x1f589; &#x1f5ba;
* [Twisted 8 bra](figures/fashion/t8bra/) &#x1f589;
* [String bikini](figures/fashion/strbik/) &#x1f589; &#x1f5ba;

## Literature

### Books

* Clifford W. Ashley, *The Ashley Book of Knots*, Doubleday, New York, 1944. ISBN 0-385-04025-3
* Lee Harrington, *Shibari You Can Use*, Mystic Productions Press, 2014. ISBN 978-0-9778727-2-5
* Lee Harrington, *More Shibari You Can Use*, Mystic Productions Press, 2014. ISBN 978-0-9778727-5-6
* Douglas Kent, *Complete Shibari: Land*, 2010. ISBN 978-0-973668-81-0
* Douglas Kent, *Complete Shibari: Sky*, 2010. ISBN 978-0-973668-82-7
* *Two Knotty Boys Showing You the Ropes*, Green Candy Press, 2007. ISBN 978-1-937866-17-4
* *Two Knotty Boys Back on the Ropes*, Green Candy Press, 2009. ISBN 978-1-931160-69-8
* Shin Nawakiri, *Essence of Shibari*, Mystic Productions Press, 2014. ISBN 978-1-942733-85-0
* Seito Saiki, *Shibari*, CreateSpace Independent Publishing Platform, 2018. ISBN 978-1-722817-71-8

### Online resources

* [The Duchy](http://www.theduchy.com/tutorials>)
* [Crash Restraint](http://crash-restraint.com/ties)
* [Rory's Brainworks](http://www.youtube.com/c/RorysBrainworks)
* [Two Knotty Boys](http://knottyboys.com/code/downloads.php)
* [deGiotto Rope](http://degiottorope.com/blogs/information/shibari-tutorial-videos)
* [Beautiful Bondage](http://www.beautifulbondage.net/store2/index.php?route=information/information&information_id=13)
* [Shibari Study](http://shibaristudy.com/catalog)
* [Twisted Monk](http://www.twistedmonk.com/pages/how-to-videos)
* [Epic Rope](http://www.youtube.com/channel/UCM5WPHGRarPW3XtKgzNFYvg)
* [Self Suspension](http://www.selfsuspend.com/video-tutorials)
* [MisterLeto](http://www.youtube.com/channel/UCaQK2Wjja9iQW3pgFh1Aqkw)
* [Rope Sensei](http://ropesensei.com)

## Technical background

See the [GitLab project](https://gitlab.com/YpsilonOmega/shibari/) for source code, updates and the possibility to contribute.
