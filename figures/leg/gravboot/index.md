# Gravity boot

## Description

The gravity boot can be used simply as a decorative pattern on the foot, but also as an anchor point to hold one leg in a suspension.

## Instructions

#. Start by loosely attaching the center bight to the foot with a lark's head (ABOK #5) as shown on the picture below. This will only temporarily hold the center bight until it will be used to finish the tie.

	![Temporary attachment of the center bight.](start.png)

#. Cross diagonally over the top of the foot from A<sub>1</sub> to B<sub>1</sub> towards the ankle.
#. Wrap the rope horizontally around the back of the lower leg from B<sub>1</sub> to C<sub>1</sub>.
#. Cross diagonally over the top of the foot again from C<sub>1</sub> to D<sub>1</sub>, thereby also crossing over the strand A<sub>1</sub>-B<sub>1</sub>.
#. Wrap the rope below the foot from D<sub>1</sub> to A<sub>2</sub>.
#. Repeat the steps above such that on the top if the foot an alternating weaving pattern is created, while behind the ankle and under the foot the wraps are parallel, as shown in the pictures, towards Y.

	![Weaving pattern on front of the foot.](front.png)

	![Wraps behind the foot.](wrap.png)

#. Remove the temporary lark's head holding the center bight.
#. Wrap the long ends from D<sub>n</sub> to X once around the short ends with the center bight.
#. Cross the center bight once over the crossing side of the long ends, below all wraps under the foot, and through the same loop again towards Y.
#. The finishing knot will resemble a [Somerville bowline knot](../../single/somerville/).

	![Finishing the tie with a Somerville bowline knot.](back.png)

## References

* <http://www.theduchy.com/gravity-boot/>
