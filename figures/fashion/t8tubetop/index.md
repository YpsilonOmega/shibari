# Twisted 8 tube top

## Description

This tie can be worn as a rope fashion resembling the shape of a tube top. It can be tied either densely, such that it covers the area, or with a regular distance between the ropes.

## Instructions

#. Start at the center of the back and make a wrap around the lower part of the chest from A to B.
#. On the back, create a lark's head (ABOK #5) by pulling the ends through the center bight and reversing direction towards C.
#. Make a wrap around the upper part of the chest from C to D.
#. On the back, pull the rope ends through the bight B-C and upwards towards the left shoulder E.
#. In the front, cross over the strand C-D by making a crossing knot (ABOK #1173).
#. Cross down over the strand A-B.
#. Cross back up under the strand A-B such that the rope comes out towards the outer side of the body.
#. Cross over the line that comes from the crossing knot towards the inner side of the body.
#. Cross up over the strand C-D.
#. Cross back up under the strand C-D such that the rope comes out towards the outer side of the body.
#. Cross over the line that comes from the previous crossing towards the inner side of the body.
#. Continue the same pattern until you have reached the right side of the body, adjusting the distance between the diagonal ropes at your will. Make sure to not use too much tension, otherwise you will pull the horizontal strands closer together and one side of the top will be narrower than the other.
#. To finish the diagonal pattern, cross up over the strand C-D with a crossing knot (ABOK #1173) towards the right shoulder F.
#. The front should be similar to the following picture:

	![Front view.](front.png)

#. On the back, cross over the strand from E, under the strand from D, over the vertical stem and under the strand from C.
#. Use up any excess rope by repeatedly crossing over F to the left, under E to the left, over E to the right and under F to the right.
#. The back shows the following picture:

	![Back view.](back.png)

## References

* <http://www.theduchy.com/twisted-8-tube-top/>
