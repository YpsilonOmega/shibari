# Pentagram

## Description

The pentagram harness (also known as rope pentagram or pentacle tie) is a purely decorative chest tie, which creates the shape of a pentagram. It is very easy to tie and well suitable for beginners.

## Instructions

#. Start with &approx; 9m or more of rope.
#. Place the center bight at the middle of the back, and lay the rope around the lower part of the chest from H to G.
#. Pull the ends through the center bight and then over the right shoulder A.
#. On the front, go first under, then over the strand G-H and then back over the left shoulder B.
#. On the back, go through the center bight from B to E.
#. Lay the ropes around the upper part of the chest from E to F, going under the strand from A and over the strand from B.
#. On the back, go through the center bight from F to C.
#. On the front, go over the strand from A, first under and then over the strand from B on the left shoulder and around the back side of the neck.
#. Go first over, then under the strand from A on the right shoulder, then over the strand from C, under the strand from B and to D.
#. On the back, go into the center bight and through under the strands from B and A.
#. Go back over the center bight and the strands from A and B, and make a half hitch (ABOK #48) over the strand from D. The final tie should look like this:

	![Pentagram - front view.](front.png)

	![Pentagram - back view.](back.png)

## References

* <http://www.knottyboys.com/videos/Harnesses/Pentagram_harness.wmv>
* <http://www.theduchy.com/pentagram-harness-1/>
