# X friction

## Description

This type of friction is useful to create a T-shaped structure. It often occurs on the top line of a takate kote or in a futomomo. As for all frictions, it should be made as tight and compact as possible.

## Instructions

#. The typical starting position for an X friction is with a rope coming from an attachment point A, and then making two turns from B to B and C to C around a fixed column, where the strand from B to C crosses over that from A to B.
#. Cross under the strand coming from A, and pull it to the upper right, forming a right angle.
#. Cross diagonally to the upper right over all strands, so that you end up over the right strand coming from B.
#. Cross down below the strands coming from B and C.
#. Cross diagonally to the upper left over all strands, so that you end up over the left strand going to B.
#. Cross down below the strands going to B and C.
#. Cross to the right over the strand coming from A.
#. Reverse direction and go back under the strand coming from A.
#. Continue in the same direction if you plan to make another tie. The result should resemble the figure below.

	![Completed X friction.](frix.png)

## References

* <http://www.theduchy.com/frictions/>
