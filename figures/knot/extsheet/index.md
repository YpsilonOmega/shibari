# Extending rope: sheet bend method

## Description

This method to extend rope is particularly useful for whipped rope ends, which have no knots. It uses the sheet bend, whose principle is to use the rope tension in order to hold the free end tight.

## Instructions

#. Take the center bight of the new rope A-B, lay it on the old rope coming from Y and pull the free ends X of the old rope through:

	![First step: pull the old rope through the center bight of the new one](stage1.png)

#. Lay the free ends X of the old rope around behind the new rope A-B:

	![Second step: free ends of the old rope behind the new one](stage2.png)

#. Lay the free ends X of the old rope further around in front of the new rope A-B, pull them through the loop thus formed and tighten up to form a sheet bend (ABOK #66):

	![Third step: rope ends through the loop and tightened](stage3.png)

## References

* <http://crash-restraint.com/ties/22>
