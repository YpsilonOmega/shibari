# Shinju

## Description

The shinju (from Japanese &#x771f;&#x73e0; - pearl) is a classical Japanese chest tie. See also the [bikini harness](../bikini/) for a similar front.

## Instructions

### Classical shinju

#. Place the center bight at the middle of the back.
#. Lay the rope around the upper part of the chest from C to D, following the front view:

	![Shinju - front view.](front.png)

#. On the back, pull the rope through the center bight to form a lark's head (ABOK #5).
#. Lay the rope once more around the upper part of the chest from F to E, closely under the strand C-D.
#. Pull the rope through the bight D-F and secure if with a half hitch (ABOK #48) over the strands from C and E. The back should look like this:

	![Shinju - before folding.](bflip.png)

#. Fold the upper strand of the half hitch down, so that the back looks like this:

	![Shinju - after folding.](aflip.png)

#. About 10cm under the half hitch, make a 90° turn, hold the rope there and go towards H.
#. Lay the rope around the lower part of the chest, from H to G.
#. On the back, pull the rope through the 90° bight from H and go back to I.
#. Lay the rope once more around the lower part of the chest from I to J, closely under the strand G-H.
#. Pull the rope through the bight G-I and make a half hitch around the strands H and J.
#. Wrap the rope once around the strand joining the upper and lower knot.
#. Pull the rope under the strands C and E and to the right shoulder A.
#. On the front, go over all strands C-D, E-F, G-H, I-J to the center of the lower part of the chest.
#. Return the rope under the strands I-J and G-H, cross over the strand from A, and under the strands E-F and C-D to the left shoulder B.
#. On the back, go under the strands D and F and over the stem (the two strands joining the upper and lower knot.
#. Secure the rope with two half hitches over the strands C and E. The final back side will look like this:

	![Shinju - final stage, back view.](final.png)

### Alternative front part

Tie the back of the shinju as above, but replace the central rope part (going from A to B) with the following sequence:

#. Coming from A, ross over the strands C-D and E-F and under the strands G-H and I-J towards the center of the lower part of the chest.
#. Move outwards, then cross upwards over the strands I-J and G-H and under the strands E-F and C-D.
#. Continue to the opposite side of the chest, crossing over the strand coming from A.
#. Cross downwards under the strands C-D and E-F and over the strands G-H and I-J.
#. Move inwards, and upwards between the first and last vertical strand (in the order of creation) under the strands I-J and G-H, over the strands E-F and C-D and under the horizontal strand connecting the left and right half of the figure towards the left shoulder B. The result will look like this:

	![Shinju - front view in case of alternative front.](altfront.png)

## References

* <http://crash-restraint.com/ties/17> (classical shinju)
* <http://www.deviantart.com/andr345r/art/Shibari-Duotone-788346676> (inspiration for the alternative front, not a shinju)
