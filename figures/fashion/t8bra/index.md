# Twisted 8 bra

## Description

This bra is inspired by the [twisted 8 tube top](../t8tubetop/), which features a similar pattern. The main difference is that the bra version does not have a continuous horizontal support line across the upper part of the chest, but two separate parts, leaving a space in the center. Also the weaving pattern is not tied from one side to the other, but from the center to the sides, so that it is easier to tie it symmetrically.

## Instructions

### Cross chest variant

In this variant, the upper support lines cross in the center of the chest, and are supported by shoulder straps on the outer sides. This creates a diamond-shaped open space in the center, whereas the woven cups have a rectangular shape.

#. Start with the center bight in the middle of the back and make a wrap around the lower part of the chest from A to B.
#. Pull the rope ends through the center bight, forming a lark's head (ABOK #5).
#. Fix the tension of the wrap with a half hitch (ABOK #48). This wrap should be tight enough to stay in position when weaving the front.
#. Continue towards the front under the left arm at C, then to the back over the right shoulder at D.
#. Cross once around the back of the neck, then to the front again over the left shoulder at E and to the back under the right arm at F.
#. In the center of the back, make another half hitch (ABOK #48) across the strand towards C, forming a vertical stem in the middle between the two half hitches.
#. Split the rope ends and run them to the front over the shoulders on each side G<sub>1</sub> and G<sub>2</sub>.
#. In the front, attach each running end to the strand C / F near the outer part of the chest with a cow hitch (ABOK #1673).
#. Run the rope ends back to the back over the shoulders at H<sub>1</sub> and H<sub>2</sub>.
#. On the back, pass the rope ends under the horizontal lines from C and F.
#. Attach the ends in front of the vertical stem with a square knot (ABOK #1204).
#. The back should now resemble the following figure.

	![Back view (cross chest variant).](backcross.png)

#. Take another rope and attach its center bight to the lower wrap in the middle of the chest using a cow hitch (ABOK #1673).
#. Weave each rope end around the upper and lower wraps on one side, from the inside outside, following the "twisted 8" pattern.
#. Once you have reached the cow hitch attaching the shoulder strap, fix the rope end with a half hitch (ABOK #48).
#. A short amount of excess rope can be hidden under or inside the weaving pattern.
#. The front will resemble the following figure.

	![Front view (cross chest variant).](frontcross.png)

### Open variant

In this variant the upper support lines do not cross, and the whole shape is connected in the center only at the lower edge, leaving a larger open space in the center. It does not need additional shoulder straps, and the tension on the support lines will cause a more triangular shape of the cups.

#. Start with the center bight in the middle of the back and make a wrap around the lower part of the chest from A to B.
#. Pull the rope ends through the center bight, forming a lark's head (ABOK #5).
#. Fix the tension of the wrap with a half hitch (ABOK #48). This wrap should be tight enough to stay in position when weaving the front.
#. Continue towards the front under the left arm at C, then to the back over the left shoulder at D.
#. Cross once around the back of the neck, then to the front again over the right shoulder at E and to the back under the right arm at F.
#. In the center of the back, make another half hitch (ABOK #48) across the strand towards C to lock the tie.

	![Back view (open variant).](backopen.png)

#. Take another rope and attach its center bight to the lower wrap in the middle of the chest using a cow hitch (ABOK #1673).
#. Weave each rope end around the upper and lower wraps on one side, from the inside outside, following the "twisted 8" pattern.
#. Once you have reached the end of the rope or the outer side of the chest, fix the rope end with a half hitch (ABOK #48).
#. A short amount of excess rope can be hidden under or inside the weaving pattern.
#. The front will resemble the following figure.

	![Front view (open variant).](frontopen.png)

### Reducing neck pressure

If the tie is worn for longer time, the pressure caused by the support line on the back of the neck can become uncomfortable. In order to reduce the pressure, any excess rope can be used to attach the neck strap to the wraps on the back.
