# Futomomo

## Description

The futomomo (from Japanese &#x5928; - fat, &#x80a1; - leg, &#x5928;&#x80a1; - thigh; also misinterpreted due to the similar pronunciation as &#x6843; - peach and attributed to the "peach-like" pattern on the thigh) is a classical single leg tie, which ties the ankle to the thigh. It comes in different variations. This is the most common version. It can be used as a floor tie, but is not stable enough for suspension.

## Instructions

#. Bring the ankle as close to the thigh as possible, in order for the tie to become tight enough, and to keep it from getting loose later.
#. Start with a single column tie above the ankle. This should not be too tight and feel comfortable.
#. Wrap the rope around the upper and lower leg, tying them together, forming a spiral with constant distance between neighboring wraps. This should look as in the figure below.

	![Forming a spiral around the leg.](spiral.png)

#. Once you have reached the knee, cross the rope ends down over the last wrap, in the middle where the calf and the thigh meet.
#. Cross up again behind the last wrap, such that the rope ends come out inside the acute angle between the last wrap and the downward crossing rope.
#. Cross horizontally, parallel to the last wrap, over the crossing vertical rope.
#. Pass the rope ends down behind the last wrap. This will create a crossing knot (ABOK #1173).
#. Use the same crossing knot to cross over all wraps on one side of the leg, until you come out at X in the figure below.

	![Crossing the rope down on one side.](down.png)

#. Pass the rope ends through to the other side of the leg, between the calf and the thigh.
#. Use crossing knots (ABOK #1173) again to cross every wrap on that side, now upwards towards the knee.
#. After crossing over the last wrap, tuck the rope ends under the horizontal part of the crossing know, back where the rope came from, thus forming a cow hitch (ABOK #1673). This will look as in the figure below (now seen from the other side).

	![Crossing the rope up on the other side.](up.png)

## References

* <http://www.theduchy.com/spiral-futomomo/>
