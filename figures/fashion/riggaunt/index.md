# Rigger gauntlet

## Description

This is a simple, yet cool looking, purely decorative tie on the forearms, to be worn at any occasion, let it be a rope event or otherwise.

## Instructions

#. Start with a lark's head (ABOK #5) and make a single wrap around the wrist from A to A.
#. Reverse direction and make another wrap from B to B around the forearm, such that the new wrap is directly next the previous one, but up along the forearm.
#. Pull the rope ends from below through the bight formed when you reversed direction in the previous step. This will effectively turn the previous wrap from B to B into a half hitch (ABOK #48) around the forearm.
#. Continue with further half hitches from C to C, and so on, further up the forearm, keeping the wraps next to each other.
#. After the last half hitch from D to D, reverse direction, go under the last wrap coming from D, then back over it and pull through the just formed bight towards X, forming a closing half hitch (ABOK #48).
#. The result should look as in the figure.

	![Final view of the rigger gauntlet.](gauntlet.png)

## References

* <http://www.knottyboys.com/videos/Decorative_Ideas/Rigger_Gauntlets.wmv>
