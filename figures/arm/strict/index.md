# Extra strict armbinder

## Description

This tie is a very restrictive and stable arm restraint. Appropriately tied it is nearly inescapable.

## Instructions

#. Start with a fixed point in the middle between the shoulders. For example, a [quick shoulder harness](../../chest/shoulder/) can be used, where the last half hitch (ABOK #48) can be omitted, since tension is maintained on the end X, as in the figure below.

	![Quick shoulder harness.](../../chest/shoulder/qsh.png)

#. From the shoulder harness, go straight down directly under the shoulder, make a right angle, cross over the left arm, wrap around and come back under the armpit, as shown in the next figure.

	![First wrap around one arm.](start.png)

#. Cross horizontally behind the vertical stem towards the right arm.
#. Cross over the right arm, wrap around and come back under the armpit, as shown in the figure below.

	![First wrap around second arm.](next.png)

#. Cross again horizontally behind the vertical stem, then down in front of the horizontal lines. This should produce the figure below.

	![First wrap completed.](first.png)

#. Reverse direction, and cross now over the right arm again, wrap around and come back between the arm and the upper body.
#. Cross horizontally behind the vertical stem towards the left arm.
#. Cross over the left arm, wrap around and come back between the arm and the upper body.
#. Cross again horizontally behind the vertical stem, then down in front of the horizontal lines. This should produce the figure below.

	![Second wrap completed.](second.png)

#. Continue the same pattern down the arms, alternating between first going around either the left or the right arm, and second over the other one. Make sure to tie not too close to the elbows and leave a larger gap there.
#. After the last wrap above the wrists, make another turn around the horizontal lines to hold the tension. The result should look as in the figure below.

	![Last wrap completed.](last.png)

#. Cross up behind all arm wraps and the quick shoulder harness, and pull tight.
#. Reverse direction and cross down in front of the quick shoulder harness and those wraps which are on the upper arm only.
#. Cross up again behind the wraps and the quick shoulder harness, to tighten the upper wraps separately from the lower ones.
#. Cross down in front on the quick shoulder harness and the upper arm wraps again.
#. Lock the tie with two half hitches (ABOK #48) on the vertical stem which connects the lowermost upper arm wrap and the uppermost forearm wrap. This should produce the figure below.

	![Final result.](final.png)

## References

* <http://crash-restraint.com/ties/38>
